public class Book extends Item implements Borrowable {
    private String author;

    public Book() {
    }

    public Book(String name, String author, double cost, int quantity) {
        setName(name);
        this.author = author;
        setCost(cost);
        setQuantity(quantity);
    }

    @Override
    public String toString(){
        return "Book [name: " + getName() + ", author: " + this.author + 
                    ", cost: " + getCost() + ", quantity: " + getQuantity() + "] \n";
    }
}