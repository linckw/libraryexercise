public class CD extends Item implements Borrowable {
    private String artist;

    public CD() {
    }

    public CD(String name, String artist, double cost, int quantity) {
        setName(name);
        this.artist = artist;
        setCost(cost);
        setQuantity(quantity);
    }

    @Override
    public String toString(){
        return "CD [name: " + getName() + ", artist: " + this.artist + 
                ", cost: " + getCost() + ", quantity: " + getQuantity() + "] \n";
    }
}