public class Periodical extends Item{
    private String publisher;

    public Periodical() {
    }

    public Periodical(String name, String publisher, double cost, int quantity) {
        setName(name);
        this.publisher = publisher;
        setCost(cost);
        setQuantity(quantity);
    }

    @Override
    public String toString(){
        return "Periodical [name: " + getName() + ", publisher: " + this.publisher + 
                ", cost: " + getCost() + ", quantity: " + getQuantity() + "] \n";
    }
}